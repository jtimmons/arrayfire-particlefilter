/* -*- mode: c++ -*- */
#pragma once

class ParticleFilter
{
public:
  ParticleFilter( ) {}
  ~ParticleFilter( ) {}

  /**
   * Initialize particle weights and states (problem dependent).
   */
  virtual void initialize() = 0;

  /**
   * Predict the state of the particles forward one timestep.
   */
  virtual void predict() = 0;

  /**
   * Update the weights of the particles based on the measurement of the new state.
   */
  virtual void update( af::array measurement ) = 0;

protected:

  /**
   * Resample the particles based upon their weights.
   *
   * Creates a CDF so that we have a weighted distribution to sample from, we will
   * then generate a set of random variables and see where in the CDF we fall - we
   * can use that information to index into (sample from) the current particle weights.
   * This is best understood with an example, below:
   *
   * Given particles w/ weights: [a:6, b:2, c:4, d:5, e:1, f:10, g:1]
   *
   *     a------b--c----d-----e-f----------g-        Particles weights w/ distance
   *      |    |     |     |     |    |     |
   *      h    i     j     k     l    m     n        Samples
   *
   * Note that samples fall into these "buckets", and will get grouped with the
   * particle to their left (the particle whose weight bucket they fell into). This will
   * yield a new set of particle weights of:       [h:6, i:6, j:4, k:5, l:10, m:10, n:1].
   *
   * This has the effect of clustering the set of particles around the ones which are most
   * likely to be the truth values.
   */
  void resample()
  {
    // Normalize weights so that they sum to 1
    af::array normalized_weights = mParticleWeights / af::sum( mParticleWeights ).scalar<float>();

    // Create cumulative sum up to 1 - then monte-carlo that with random values on [0, 1]
    af::array cdf = af::accum( normalized_weights );
    af::array monte_carlo = af::randu( mNumParticles );

    // Sample from the original weight list where monte carlo values fall in the cdf
    af::array new_particle_weights = af::array( mNumParticles );
    for ( int i=0; i < mNumParticles; ++i )
    {
      af::array idx = af::where( monte_carlo(i).scalar<float>() < cdf )(0);

      // Don't want to underflow if we match against the first element
      if ( af::allTrue<bool>( idx > 0 ) )
      {
	idx -= 1;
      }

      new_particle_weights(i) = mParticleWeights(idx);
    }

    mParticleWeights = new_particle_weights;
  }

  int mNumParticles;
  int mNumStateVars;
  af::array mParticleWeights;
  af::array mParticleStates;
};
